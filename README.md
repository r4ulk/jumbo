# README #

This a test challenge Jumbo using spring-boot and gradle build tasks

### About the challenge ###

Create an application that shows the 5 closest Jumbo stores to a given position.
You can find a list of Jumbo stores in JSON format attached to this document.

#### There are just a few rules: ####

* It has to be a Java application. What frameworks, libraries you use etc is up to you.
* It has to be a web application. We would like to see at least an API, having an HTML/Javascript frontend is a nice to have.
* Write your code as if it’s production code as much as possible.
* Make sure the reviewer can easily run the application for evaluation purposes.

### How do I get set up? ###

* Run the followed codes in your terminal:
```
npm i npm to update
npm install
./gradlew build && java -jar build/libs/jumbo-0.1.0.jar
```
If you have some problem with npm dependencies, try to run to install manually dependencies:
```
npm install webpack --save
npm install babel-core babel-loader babel-preset-es2015 babel-preset-react --save
npm install react react-dom --save
npm install webpack-dev-server --dev-install
./node_modules/.bin/webpack -d
```
and again to up spring-boot with gradle:
`./gradlew build && java -jar build/libs/jumbo-0.1.0.jar`

### Lets try ###

API Jumbo Stores
`http://localhost:8080/jumbo/stores/?longitude=5.477477&latitude=51.468220`

Home
`http://localhost:8080`

### Credits ###

Raul Klumpp <raulklumpp@gmail.com>