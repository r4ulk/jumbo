package com.jumbo.service;

import com.jumbo.model.store.Store;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class DataLoaderServiceTest {

    @Autowired
    DataLoaderService dataLoader;

    @Test
    public void shouldLoadStores() throws Exception {
        List<Store> stores = dataLoader.get();
        assertThat(stores).isNotNull();
        assertThat(stores.size()).isEqualTo(587);
        assertThat(stores.get(0).getAddressName()).isEqualTo("Jumbo 's Gravendeel Gravendeel Centrum");
    }

}
