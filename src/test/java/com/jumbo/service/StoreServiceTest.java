package com.jumbo.service;

import com.jumbo.model.location.Location;
import com.jumbo.model.store.Store;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class StoreServiceTest {

    @Autowired
    StoreService storeService;

    private static final Float LONGITUDE = 5.477477F;
    private static final Float LATITUDE  = 51.468220F;

    private Location location;

    @Before
    public void init() {
        this.location = new Location(LONGITUDE, LATITUDE);
    }

    @Test
    public void shouldGetFiveNearbyStores() throws Exception {
        List<Store> stores = storeService.get(LONGITUDE, LATITUDE);
        assertThat(stores).isNotNull();
        assertThat(stores.size()).isEqualTo(5);
        assertThat(stores.get(0).getDistance()).isEqualTo(Double.valueOf(0.0));
    }
}
