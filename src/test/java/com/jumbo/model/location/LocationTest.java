package com.jumbo.model.location;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class LocationTest {

    private static final Float LONGITUDE = 5.477477F;
    private static final Float LATITUDE  = 51.468220F;

    private static final Float LONGITUDE_TO = 5.463269F;
    private static final Float LATITUDE_TO  = 51.470731F;

    private Location location;

    private Location locationTo;

    @Before
    public void init() {
        this.location = new Location(LONGITUDE, LATITUDE);
        this.locationTo = new Location(LONGITUDE_TO, LATITUDE_TO);
    }

    @Test
    public void shouldCalcDistanceBetweenSameLocation() throws Exception {
       assertThat(location).isNotNull();
       assertThat(location.distance(location)).isEqualTo(Double.valueOf(0.0));
    }

    @Test
    public void shouldCalcDistanceBetween() throws Exception {
        assertThat(location).isNotNull();
        assertThat(locationTo).isNotNull();
        assertThat(location.distance(locationTo)).isEqualTo(Double.valueOf(1.0228957629848319));
    }

}
