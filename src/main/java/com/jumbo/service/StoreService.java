package com.jumbo.service;

import com.jumbo.exception.LocationException;
import com.jumbo.model.store.Store;

import java.util.List;

public interface StoreService {

    List<Store> get(Float longitude, Float latitude) throws LocationException;
}
