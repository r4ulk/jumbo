package com.jumbo.service;

import com.jumbo.model.store.Store;

import java.util.List;

public interface DataLoaderService {

    void load();

    List<Store> get();
}
