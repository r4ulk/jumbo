package com.jumbo.service.impl;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jumbo.model.store.Store;
import com.jumbo.service.DataLoaderService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

@Service
public class DataLoaderServiceImpl implements DataLoaderService {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    private List<Store> stores;

    @Autowired
    ObjectMapper jsonMapper;

    @Override
    public void load() {
        TypeReference<List<Store>> typeReference = new TypeReference<List<Store>>() {
        };
        InputStream inputStream = TypeReference.class.getResourceAsStream("/stores.json");
        try {
            this.stores = jsonMapper.readValue(inputStream, typeReference);
            logger.info("Stores data loaded...");
        } catch (IOException e) {
            System.out.println(e);
            logger.error("Couldnt read stores from json. Message: ", e.getMessage());
        }
    }

    @Override
    public List<Store> get() {
        return this.stores;
    }

}
