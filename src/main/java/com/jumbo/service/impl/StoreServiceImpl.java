package com.jumbo.service.impl;

import com.jumbo.exception.LocationException;
import com.jumbo.model.location.Location;
import com.jumbo.model.store.Store;
import com.jumbo.service.DataLoaderService;
import com.jumbo.service.StoreService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class StoreServiceImpl implements StoreService {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    DataLoaderService dataLoader;

    @Override
    public List<Store> get(Float longitude, Float latitude) throws LocationException {
        if(longitude == null || latitude == null) {
            throw new LocationException("Latitude or Longitude is empty or null");
        }
        Location location  = new Location(longitude, latitude); // initial location
        List<Store> all    = dataLoader.get(); // get all stores
        // filter stores in radius
        List<Store> stores = all.stream()
                .filter(s -> location.inRadius(new Location(s.getLongitude(), s.getLatitude())))
                .collect(Collectors.toList());
        // calc and set distance
        stores.forEach(s -> s.setDistance(location.distance(new Location(s.getLongitude(), s.getLatitude()))));
        // sort by distance asc
        stores.sort(Comparator.comparing(s -> s.getDistance()));
        // return only 5 (five) stores
        return stores.size() > 5 ? stores.subList(0, 5) : stores;
    }

}
