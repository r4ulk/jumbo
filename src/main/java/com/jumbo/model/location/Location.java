package com.jumbo.model.location;

import com.jumbo.exception.LocationException;

public class Location {

    private final static double EARTH_RADIUS = 6.371; // Earth Radius aprox in KM

    private Float latitude;

    private Float longitute;

    public Location(Float longitute, Float latitude) {
        this.longitute = longitute;
        this.latitude = latitude;
    }

    public Float getLongitute() {
        return longitute;
    }

    public void setLongitute(Float longitute) {
        this.longitute = longitute;
    }

    public Float getLatitude() {
        return latitude;
    }

    public void setLatitude(Float latitude) {
        this.latitude = latitude;
    }

    /*
        Check if a location is in radius
     */
    public boolean inRadius(Location locationTo) {
        double C = 40075.04, A = 360*EARTH_RADIUS/C, B = A/Math.cos(Math.toRadians(this.latitude));
        return Math.pow((locationTo.getLatitude()-this.latitude)/A, 2)
                + Math.pow((locationTo.getLongitute()-this.longitute)/B, 2) < 1;

    }

    /*
        Return distance in km between 2 locations
     */
    public double distance(Location locationTo) {
        double theta = this.longitute - locationTo.getLongitute();
        double dist = Math.sin(deg2rad(this.latitude)) * Math.sin(deg2rad(locationTo.getLatitude())) + Math.cos(deg2rad(this.latitude)) * Math.cos(deg2rad(locationTo.getLatitude())) * Math.cos(deg2rad(theta));
        dist = Math.acos(dist);
        dist = rad2deg(dist);
        dist = dist * 60 * 1.1515;
        return dist * 1.609344;
    }

    /*
        Converts decimal degrees to radians
     */
    private double deg2rad(double deg) {
        return (deg * Math.PI / 180.0);
    }

    /*
        Converts radians to decimal degrees
     */
    private double rad2deg(double rad) {
        return (rad * 180.0 / Math.PI);
    }

    @Override
    public String toString() {
        return "Location{" +
                "latitude=" + latitude +
                ", longitute=" + longitute +
                '}';
    }
}
