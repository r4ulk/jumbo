package com.jumbo.model.store;

public class Store {

    private String city;

    private String postalCode;

    private String street;

    private String street2;

    private String street3;

    private String addressName;

    private String uuid;

    private Float longitude;

    private Float latitude;

    private String complexNumber;

    private Boolean showWarningMessage;

    private String locationType;

    private Boolean collectionPoint;

    private Long sapStoreID;

    private String todayOpen;

    private String todayClose;

    private double distance;

    public Store() {
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getStreet2() {
        return street2;
    }

    public void setStreet2(String street2) {
        this.street2 = street2;
    }

    public String getStreet3() {
        return street3;
    }

    public void setStreet3(String street3) {
        this.street3 = street3;
    }

    public String getAddressName() {
        return addressName;
    }

    public void setAddressName(String addressName) {
        this.addressName = addressName;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public Float getLongitude() {
        return longitude;
    }

    public void setLongitude(Float longitude) {
        this.longitude = longitude;
    }

    public Float getLatitude() {
        return latitude;
    }

    public void setLatitude(Float latitude) {
        this.latitude = latitude;
    }

    public String getComplexNumber() {
        return complexNumber;
    }

    public void setComplexNumber(String complexNumber) {
        this.complexNumber = complexNumber;
    }

    public Boolean getShowWarningMessage() {
        return showWarningMessage;
    }

    public void setShowWarningMessage(Boolean showWarningMessage) {
        this.showWarningMessage = showWarningMessage;
    }

    public String getLocationType() {
        return locationType;
    }

    public void setLocationType(String locationType) {
        this.locationType = locationType;
    }

    public Boolean getCollectionPoint() {
        return collectionPoint;
    }

    public void setCollectionPoint(Boolean collectionPoint) {
        this.collectionPoint = collectionPoint;
    }

    public Long getSapStoreID() {
        return sapStoreID;
    }

    public void setSapStoreID(Long sapStoreID) {
        this.sapStoreID = sapStoreID;
    }

    public String getTodayOpen() {
        return todayOpen;
    }

    public void setTodayOpen(String todayOpen) {
        this.todayOpen = todayOpen;
    }

    public String getTodayClose() {
        return todayClose;
    }

    public void setTodayClose(String todayClose) {
        this.todayClose = todayClose;
    }

    public double getDistance() {
        return distance;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }

    @Override
    public String toString() {
        return "Store{" +
                "city='" + city + '\'' +
                ", postalCode='" + postalCode + '\'' +
                ", street='" + street + '\'' +
                ", street2='" + street2 + '\'' +
                ", street3='" + street3 + '\'' +
                ", addressName='" + addressName + '\'' +
                ", uuid='" + uuid + '\'' +
                ", longitude=" + longitude +
                ", latitude=" + latitude +
                ", complexNumber='" + complexNumber + '\'' +
                ", showWarningMessage=" + showWarningMessage +
                ", locationType='" + locationType + '\'' +
                ", collectionPoint=" + collectionPoint +
                ", sapStoreID=" + sapStoreID +
                ", todayOpen='" + todayOpen + '\'' +
                ", todayClose='" + todayClose + '\'' +
                ", distance=" + distance +
                '}';
    }
}
