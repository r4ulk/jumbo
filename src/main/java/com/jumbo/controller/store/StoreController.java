package com.jumbo.controller.store;

import com.jumbo.exception.LocationException;
import com.jumbo.model.store.Store;
import com.jumbo.service.StoreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "/jumbo")
public class StoreController {

    @Autowired
    StoreService storeService;

    /**
     * Get Jumbo Store list
     * @param longitude - Float longitude
     * @param latitude  - Float latitude
     * @return 5 (five) Jumbo store list
     */
    @RequestMapping(value = "/stores", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Store> get(@RequestParam Float longitude, @RequestParam Float latitude) throws LocationException {
        return storeService.get(longitude, latitude);
    }

}
